# asyncpg_utility

#### Extensions to the [asyncpg postgres driver](https://github.com/MagicStack/asyncpg) for python (3); notably, a NamedParameterQuery and NamedParameterConnection class that support named parameters (as opposed to positional ones: $1, $2, ...)

#### [Editorial aside: positional arguments have always struck me as dangerous. How many prepared SQL statements are lurking in the wild where the positions of arguments passed to them are wrong, and perhaps have only been working so far thanks to the luck of the multiple values involved?]

## Code example:

```
import asyncio
import asyncpg
from asyncpg_utility import NamedParameterQuery, NamedParameterConnection
# .
# .
# .
query='SELECT * FROM my_table WHERE name={{NAME}} AND age={{AGE}}'
my_named_parameter_query=NamedParameterQuery(query)
# .
# .
# .
async def my_async_routine():
          # .
          # .
          # .
          
          # "conn" is your vanilla asyncpg database connection
          # (obtained from "await asyncpg.connect(...", or acquired from a pool...)
          my_named_parameter_conn=NamedParameterConnection(conn, my_named_parameter_query)

          # Note how the order of the arguments to NamedParameterConnection's
          # fetchrow function doesn't matter now...
          await result=my_named_parameter_conn.fetchrow(age=20, name='Mr.Big')

          # OR: my_named_parameter_conn.fetch(...
          # OR: my_named_parameter_conn.fetchval(...
          # OR: my_named_parameter_conn.execute(...
```
###### &lt;grin&gt;Of course, you could go with more meaningful variable names than the "my_..." ones used here...&lt;/grin&gt;

###### If you forget _named_ parameters, an exception will be raised (which is a Good Thing -the exception, that is; not your having made a mistake...)

###### The NamedParameterQuery constructor accepts a "case_sensitive" argument (default: False); so by setting case_sensitive to True, you could _require_ that the named parameter in the initial query value in the example above be "{{age}}"; or, correspondingly, the parameter to the fetch routine be "AGE='42'".

###### The "timeout" and "column" parameters to the NamedParameterConnection class's execute and fetch routines are class members initialized by the class's constructor (since ALL arguments to execute and fetch routines are now treated as named parameters to an embedded SQL query); so if you should need those adjusted from one fetch to the next, they can be set before the fetch. [example: my_named_parameter_connection.timeout=2000]

###### The following arguments can be set at module level so that constructor defaults change accordingly for all subsequent class instantiations:

> ###### case_sensitive (True/False, default is False)
> ###### parameter_markers (default is ['{{', '}}'])
> ###### timeout (for fetches, default is 0)
> ###### column (for fetchval's, default is 0)
> ###### close_on_err (True/False, default is True)
> ###### exit_on_err (True/False, default is True)

###### So by doing (just after import of facilities from asyncpg_utility...):
> ###### asyncpg_utility.parameter_markers=['{', '}']

###### ALL subsequent NamedParameterQuery's will use that as their default.

###### **Strategy:** you want to ideally initialize a NamedParameterQuery variable _once_ in your program; then once a connection is established (or acquired from a pool), initialize a NamedParameterConnection before using its execute or fetch member functions.

[You can reach the developer at the Contact link at [bitmodeler.com](https://bitmodeler.com)]
